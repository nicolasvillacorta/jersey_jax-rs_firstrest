package com.accenture.resources;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
public class UserResource {
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	public List<String> getUsers(){
		List<String> users = new ArrayList<>();
		users.add("Nicolas");
		System.out.println("Se llamo al GET");
		return users;
	}
	
	@POST
	@Produces({MediaType.APPLICATION_JSON})
	public Response SaveUsers(){
		
		
		return Response.ok().build();
	}
}
