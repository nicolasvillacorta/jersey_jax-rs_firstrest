package com.accenture.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JerseyTryingApplication {

	public static void main(String[] args) {
		SpringApplication.run(JerseyTryingApplication.class, args);
	}

}
