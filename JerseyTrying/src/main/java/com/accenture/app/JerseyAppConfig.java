package com.accenture.app;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

@Component
@ApplicationPath("/api")
public class JerseyAppConfig extends ResourceConfig{

	public JerseyAppConfig() {
		//Registro una clase
		//register(UserResource.class);
		
		//O puedo registrar directamente un paquete.
		packages("com.accenture.resources");	
	}
}
